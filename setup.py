from setuptools import setup
from os.path import join, dirname

# single version
__version__ = 'UNDEFINED'
# version.py should contain just the one line: __version__ = 'X.Y.Z'
with open(join(dirname(__file__), 'dcmextras', 'version.py')) as f:
    exec(f.read())


def readme(fname):
    with open(join(dirname(__file__), fname)) as f:
        text = f.read()
    return text


config = {
    'name': 'dcmextras',
    'description': 'Additional dicom tools for pydicom and Siemens',
    'long_description': readme('README.md'),
    'author': 'Ronald Hartley-Davies',
    'author_email': 'R.Hartley-Davies@bristol.ac.uk',
    'version': __version__,
    'license': 'MIT',
    'url': 'https://bitbucket.org/rtrhd/dcmextras',
    'download_url': 'https://bitbucket.org/rtrhd/dcmextras/downloads/',
    'packages': ['dcmextras'],
    'install_requires': [
        'numpy>=1.13',
        'pydicom>=2.0'
    ],
    'scripts': ['bin/dcm2js'],
    'entry_points': {
        'console_scripts': ['phoenix=dcmextras.siemensphoenix:main'],
    },
    'classifiers': [
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering :: Medical Science Apps.",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX",
        "Operating System :: Windows",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9"
    ]
}

setup(**config)
