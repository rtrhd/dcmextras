.PHONY: build install tests clean
build:
	python setup.py build

install:
	python setup.py install
	install bin/dcm2js /usr/local/bin

tests:
	(cd tests; make)

test-upload:
	python3 setup.py sdist bdist_wheel
	twine upload -r testpypi 'dist/*'

upload:
	python3 setup.py sdist bdist_wheel
	twine upload 'dist/*'
clean:
	rm -rf build/ dist/ *.egg-info

