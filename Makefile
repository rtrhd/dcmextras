.PHONY: build install tests clean
build:
	python setup.py build

install:
	python setup.py install
	install bin/dcm2js /usr/local/bin

tests:
	(cd tests; make)

clean:
	rm -rf build/ dist/ *.egg-info

